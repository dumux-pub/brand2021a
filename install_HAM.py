#!/usr/bin/env python3

#
# This installs the module brand2021a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
#
#
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-alugrid     |  origin/releases/2.8  |  ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f  |  2021-08-22 11:59:22 +0200  |
# |       dune-istl       |  origin/releases/2.8  |  fffb544a61d2c65a0d2fc7c751f36909f06be8f5  |  2021-08-31 13:58:37 +0000  |
# |       dune-grid       |  origin/releases/2.8  |  e3371946f18df31d4ad542e5e7b51f652954edbc  |  2021-10-26 11:13:47 +0000  |
# |         dumux         |     origin/master     |  b987e1509021e3cf3ce2f987406427436ae50615  |  2022-04-05 09:11:16 +0000  |
# |  dune-localfunctions  |  origin/releases/2.8  |  f6628171b2773065ab43f97a77f47cd8c4283d8f  |  2021-08-31 14:03:38 +0000  |
# |      brand2021a       |      origin/main      |  95e8371edc20f5e8ff7114076544072b1a1dc90f  |  2022-07-21 12:33:48 +0000  |
# |     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
# |     dune-foamgrid     |     origin/master     |  43bfdb6181fae187fd803eca935a030d8d5ab0bc  |  2021-07-03 20:20:08 +0000  |
# |      dune-common      |  origin/releases/2.8  |  fb179f9420efb44ba76bd6219823e436ef63122a  |  2021-09-24 05:38:46 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.8", "ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.8", "fffb544a61d2c65a0d2fc7c751f36909f06be8f5", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.8", "e3371946f18df31d4ad542e5e7b51f652954edbc", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/master", "b987e1509021e3cf3ce2f987406427436ae50615", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.8", "f6628171b2773065ab43f97a77f47cd8c4283d8f", )

print("Installing brand2021a")
installModule("brand2021a", "https://git.iws.uni-stuttgart.de/dumux-pub/brand2021a.git", "origin/main", "95e8371edc20f5e8ff7114076544072b1a1dc90f", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.8", "e7bfb66e48496aa28e47974c33ea9a4579bf723b", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "43bfdb6181fae187fd803eca935a030d8d5ab0bc", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.8", "fb179f9420efb44ba76bd6219823e436ef63122a", )

print("Applying patch for uncommitted changes in dumux")
patch = """
diff --git a/dumux/material/binarycoefficients/h2o_air.hh b/dumux/material/binarycoefficients/h2o_air.hh
index f06d6ead7..9978c4a65 100644
--- a/dumux/material/binarycoefficients/h2o_air.hh
+++ b/dumux/material/binarycoefficients/h2o_air.hh
@@ -75,7 +75,7 @@ public:

         using std::pow;
         Dgaw=Daw*(pg0/pressure)*pow((temperature/T0),Theta);
-
+ //       Dgaw=Daw*pow((temperature/T0),Theta);
         return Dgaw;
     }

diff --git a/dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh b/dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh
index 1f3a6e79b..347aa5c82 100644
--- a/dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh
+++ b/dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh
@@ -379,7 +379,7 @@ public:
         { return krwHighSwe_; }

 private:
-        S pcLowSwe_ = 0.01;
+        S pcLowSwe_ = 0.001;
         S pcHighSwe_ = 0.99;
         S krnLowSwe_ = 0.1;
         S krwHighSwe_ = 0.9;
@@ -389,7 +389,7 @@ private:
     template<class MaterialLaw>
     void init(const MaterialLaw* m, const std::string& paramGroup)
     {
-        pcLowSwe_ = getParamFromGroup<Scalar>(paramGroup, \"VanGenuchtenPcLowSweThreshold\", 0.01);
+        pcLowSwe_ = getParamFromGroup<Scalar>(paramGroup, \"VanGenuchtenPcLowSweThreshold\", 0.001);
         pcHighSwe_ = getParamFromGroup<Scalar>(paramGroup, \"VanGenuchtenPcHighSweThreshold\", 0.99);
         krwHighSwe_ = getParamFromGroup<Scalar>(paramGroup, \"VanGenuchtenKrwHighSweThreshold\", 0.9);
         krnLowSwe_ = getParamFromGroup<Scalar>(paramGroup, \"VanGenuchtenKrnLowSweThreshold\", 0.1);
diff --git a/dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh b/dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh
index 068436178..68c022b8e 100644
--- a/dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh
+++ b/dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh
@@ -74,7 +74,15 @@ public:
         const Scalar diffCoeff = volVars.diffusionCoefficient(phaseIdx, compIdxI, compIdxJ);
         const Scalar porosity = volVars.porosity();
         const Scalar sat = max<Scalar>(volVars.saturation(phaseIdx), 0.0);
+
+
+    // HARDCODED: Hier wurde für die Effektive Diffusivität die Beziehung der \"constanttortuosity\" verwendet.
+
+
         return porosity * (sat*sat*sat) * cbrt(porosity * sat) * diffCoeff;
+        //return 0.0000083*diffCoeff;
+
+       // return 0.000019*diffCoeff;// 85000 = 0.00001176 // 50000 = 0.00002
     }

 };
diff --git a/dumux/material/fluidsystems/h2oair.hh b/dumux/material/fluidsystems/h2oair.hh
index f528e4983..af232f28f 100644
--- a/dumux/material/fluidsystems/h2oair.hh
+++ b/dumux/material/fluidsystems/h2oair.hh
@@ -68,7 +68,7 @@ struct H2OAirDefaultPolicy
 template <class Scalar,
           class H2Otype = Components::TabulatedComponent<Components::H2O<Scalar> >,
           class Policy = H2OAirDefaultPolicy<>,
-          bool useKelvinVaporPressure = false>
+          bool useKelvinVaporPressure = true>
 class H2OAir
 : public Base<Scalar, H2OAir<Scalar, H2Otype, Policy> >
 {
@@ -270,9 +270,9 @@ public:
                 const auto pc =  (fluidState.wettingPhase() == (int) H2OIdx)
                                  ? fluidState.pressure(AirIdx)-fluidState.pressure(H2OIdx)
                                  : fluidState.pressure(H2OIdx)-fluidState.pressure(AirIdx);
-                return H2O::vaporPressure(t)*exp( -pc * molarMass(H2OIdx)
-                                                      / density(fluidState, H2OIdx)
-                                                      / (Dumux::Constants<Scalar>::R*t) );
+                    return H2O::vaporPressure(t)*exp( -pc * molarMass(H2OIdx)
+                                                      / H2O::liquidDensity(t, fluidState.pressure(H2OIdx))
+                                                      / (Dumux::Constants<Scalar>::R*t) );
             }
         }
         else if (compIdx == AirIdx)
diff --git a/dumux/material/solidsystems/compositionalsolidphase.hh b/dumux/material/solidsystems/compositionalsolidphase.hh
index cc604ee7b..4a77ed3dd 100644
--- a/dumux/material/solidsystems/compositionalsolidphase.hh
+++ b/dumux/material/solidsystems/compositionalsolidphase.hh
@@ -37,23 +37,27 @@ namespace SolidSystems {
  *       and cannot increase its mass by precipitation from a fluid phase.
  * \\note inert components have to come after all non-inert components
  */
-template <class Scalar, class Component1, class Component2, int numInert = 0>
+template <class Scalar, class Component1, class Component2, class Component3, class Component4, class Component5, int numInert = 0>
 class CompositionalSolidPhase
 {
 public:
     using ComponentOne = Component1;
     using ComponentTwo = Component2;
+    using ComponentThree = Component3;
+    using ComponentFour = Component4;
+    using ComponentFive = Component5;


     /****************************************
      * Solid phase related static parameters
      ****************************************/
-    static constexpr int numComponents = 2;
+    static constexpr int numComponents = 5;
     static constexpr int numInertComponents = numInert;
     static constexpr int comp0Idx = 0;
     static constexpr int comp1Idx = 1;
-
-
+    static constexpr int comp2Idx = 2;
+    static constexpr int comp3Idx = 3;
+    static constexpr int comp4Idx = 4;
     /*!
      * \\brief Return the human readable name of a solid phase
      *
@@ -65,6 +69,9 @@ public:
         {
             case comp0Idx: return ComponentOne::name();
             case comp1Idx: return ComponentTwo::name();
+            case comp2Idx: return ComponentThree::name();
+            case comp3Idx: return ComponentFour::name();
+            case comp4Idx: return ComponentFive::name();
             default: DUNE_THROW(Dune::InvalidStateException, \"Invalid component index \" << compIdx);
         }
     }
@@ -96,6 +103,9 @@ public:
         {
             case comp0Idx: return ComponentOne::molarMass();
             case comp1Idx: return ComponentTwo::molarMass();
+            case comp2Idx: return ComponentThree::molarMass();
+            case comp3Idx: return ComponentFour::molarMass();
+            case comp4Idx: return ComponentFive::molarMass();
             default: DUNE_THROW(Dune::InvalidStateException, \"Invalid component index \" << compIdx);
         }
     }
@@ -108,11 +118,17 @@ public:
     {
         Scalar rho1 = ComponentOne::solidDensity(solidState.temperature());
         Scalar rho2 = ComponentTwo::solidDensity(solidState.temperature());
+        Scalar rho3 = ComponentThree::solidDensity(solidState.temperature());
+        Scalar rho4 = ComponentFour::solidDensity(solidState.temperature());
+        Scalar rho5 = ComponentFive::solidDensity(solidState.temperature());
         Scalar volFrac1 = solidState.volumeFraction(comp0Idx);
         Scalar volFrac2 = solidState.volumeFraction(comp1Idx);
+        Scalar volFrac3 = solidState.volumeFraction(comp2Idx);
+        Scalar volFrac4 = solidState.volumeFraction(comp3Idx);
+        Scalar volFrac5 = solidState.volumeFraction(comp4Idx);

         return (rho1*volFrac1+
-               rho2*volFrac2)/(volFrac1+volFrac2);
+               rho2*volFrac2+rho3*volFrac3+rho4*volFrac4+rho5*volFrac5)/(volFrac1+volFrac2+volFrac3+rho4*volFrac4+rho5*volFrac5);
     }

     /*!
@@ -125,6 +141,9 @@ public:
         {
             case comp0Idx: return ComponentOne::solidDensity(solidState.temperature());
             case comp1Idx: return ComponentTwo::solidDensity(solidState.temperature());
+            case comp2Idx: return ComponentThree::solidDensity(solidState.temperature());
+            case comp3Idx: return ComponentFour::solidDensity(solidState.temperature());
+            case comp4Idx: return ComponentFive::solidDensity(solidState.temperature());
             default: DUNE_THROW(Dune::InvalidStateException, \"Invalid component index \" << compIdx);
         }
     }
@@ -139,6 +158,9 @@ public:
         {
             case comp0Idx: return ComponentOne::solidDensity(solidState.temperature())/ComponentOne::molarMass();
             case comp1Idx: return ComponentTwo::solidDensity(solidState.temperature())/ComponentTwo::molarMass();
+            case comp2Idx: return ComponentThree::solidDensity(solidState.temperature())/ComponentThree::molarMass();
+            case comp3Idx: return ComponentFour::solidDensity(solidState.temperature())/ComponentFour::molarMass();
+            case comp4Idx: return ComponentFive::solidDensity(solidState.temperature())/ComponentFive::molarMass();
             default: DUNE_THROW(Dune::InvalidStateException, \"Invalid component index \" << compIdx);
         }
     }
@@ -151,11 +173,17 @@ public:
     {
         Scalar lambda1 = ComponentOne::solidThermalConductivity(solidState.temperature());
         Scalar lambda2 = ComponentTwo::solidThermalConductivity(solidState.temperature());
+        Scalar lambda3 = ComponentThree::solidThermalConductivity(solidState.temperature());
+        Scalar lambda4 = ComponentFour::solidThermalConductivity(solidState.temperature());
+        Scalar lambda5 = ComponentFive::solidThermalConductivity(solidState.temperature());
         Scalar volFrac1 = solidState.volumeFraction(comp0Idx);
         Scalar volFrac2 = solidState.volumeFraction(comp1Idx);
+        Scalar volFrac3 = solidState.volumeFraction(comp2Idx);
+        Scalar volFrac4 = solidState.volumeFraction(comp3Idx);
+        Scalar volFrac5 = solidState.volumeFraction(comp4Idx);

         return (lambda1*volFrac1+
-               lambda2*volFrac2)/(volFrac1+volFrac2);
+               lambda2*volFrac2+lambda3*volFrac3+lambda4*volFrac4+lambda5*volFrac5)/(volFrac1+volFrac2+volFrac3+lambda4*volFrac4+lambda5*volFrac5);
     }

     /*!
@@ -166,11 +194,17 @@ public:
     {
         Scalar c1 = ComponentOne::solidHeatCapacity(solidState.temperature());
         Scalar c2 = ComponentTwo::solidHeatCapacity(solidState.temperature());
+        Scalar c3 = ComponentThree::solidHeatCapacity(solidState.temperature());
+        Scalar c4 = ComponentFour::solidHeatCapacity(solidState.temperature());
+        Scalar c5 = ComponentFive::solidHeatCapacity(solidState.temperature());
         Scalar volFrac1 = solidState.volumeFraction(comp0Idx);
         Scalar volFrac2 = solidState.volumeFraction(comp1Idx);
+        Scalar volFrac3 = solidState.volumeFraction(comp2Idx);
+        Scalar volFrac4 = solidState.volumeFraction(comp3Idx);
+        Scalar volFrac5 = solidState.volumeFraction(comp4Idx);

         return (c1*volFrac1+
-               c2*volFrac2)/(volFrac1+volFrac2);
+               c2*volFrac2+c3*volFrac3+c4*volFrac4+c5*volFrac5)/(volFrac1+volFrac2+volFrac3+c4*volFrac4+c5*volFrac5);
     }

 };
diff --git a/dumux/porousmediumflow/richards/volumevariables.hh b/dumux/porousmediumflow/richards/volumevariables.hh
index 9273a3e4b..295af9753 100644
--- a/dumux/porousmediumflow/richards/volumevariables.hh
+++ b/dumux/porousmediumflow/richards/volumevariables.hh
@@ -190,7 +190,7 @@ public:
                 molarDensity_[gasPhaseIdx] = IdealGas<Scalar>::molarDensity(temperature(), problem.nonwettingReferencePressure());
                 moleFraction_[liquidPhaseIdx] = 1.0;

-                moleFraction_[gasPhaseIdx] = FluidSystem::H2O::vaporPressure(temperature()) / problem.nonwettingReferencePressure();
+                moleFraction_[gasPhaseIdx] = FluidSystem::vaporPressure(fluidState_,0) / problem.nonwettingReferencePressure();

                 const auto averageMolarMassGasPhase = (moleFraction_[gasPhaseIdx]*FluidSystem::molarMass(liquidPhaseIdx)) +
                 ((1-moleFraction_[gasPhaseIdx])*FluidSystem::molarMass(gasPhaseIdx));
"""
applyPatch("dumux", patch)

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
