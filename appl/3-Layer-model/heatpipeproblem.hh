 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_PROBLEM_HH
#define DUMUX_HEATPIPE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux
{
template <class TypeTag >
class HeatPipeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem =GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using VolumeVariables = typename GridVariables::GridVolumeVariables::VolumeVariables;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

public:
    HeatPipeProblem(std::shared_ptr<const FVGridGeometry> fVGridGeometry)
    : ParentType(fVGridGeometry)
    {
        FluidSystem::init();
        this->spatialParams().plotMaterialLaw();
        diffusiveFlux_.resize(this->gridGeometry().numDofs());

            // resize to the number of vertices of the grid
        cellNum_.assign(this->gridGeometry().gridView().size(2), 0);

        for (const auto& element : elements(this->gridGeometry().gridView()))
            for (unsigned int vIdx = 0; vIdx < element.subEntities(2); ++vIdx)
                ++cellNum_[this->gridGeometry().vertexMapper().subIndex(element, vIdx, 2)];
    }

        /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */
    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.solidThermalConductivity(); }, "solidThermalConductivity");
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.effectiveDiffusionCoefficient(1/*phaseIdx*/,1/*compIIdx*/,/*compJIdx*/0); }, "effectiveDiffusionCoefficient");
    }

    void calculateAdditionalOutput(const SolutionVector& sol, const GridVariables& gridVars)
    {
        std::fill(diffusiveFlux_.begin(), diffusiveFlux_.end(), 0);

        using Velocity = Dune::FieldVector<Scalar, 2>;

        for (const auto& element : elements(this->gridGeometry().gridView(), Dune::Partitions::interior))
        {
            const auto geometry = element.geometry();
            const Dune::GeometryType geomType = geometry.type();

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, sol);

            auto elemFluxVars = localView(gridVars.gridFluxVarsCache());
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            using FluxVariables =  GetPropType<TypeTag, Properties::FluxVariables>;

            using ScvVelocities = Dune::BlockVector<Velocity>;
            ScvVelocities scvVelocities(fvGeometry.numScv());
            scvVelocities = 0;

            // get the transposed Jacobian of the element mapping
            using Dune::referenceElement;
            const auto refElement = referenceElement(geometry);
            const auto& localPos = refElement.position(0, 0);
            const auto jacobianT2 = geometry.jacobianTransposed(localPos);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (scvf.boundary())
                    continue;

                // local position of integration point
                const auto localPosIP = geometry.local(scvf.ipGlobal());

                // Transformation of the global normal vector to normal vector in the reference element
                const auto jacobianT1 = geometry.jacobianTransposed(localPosIP);
                const auto globalNormal = scvf.unitOuterNormal();
                GlobalPosition localNormal(0);
                jacobianT1.mv(globalNormal, localNormal);
                localNormal /= localNormal.two_norm();

                // instantiate the flux variables
                FluxVariables fluxVars;
                fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVars);

                // get the volume flux divided by the area of the
                // subcontrolvolume face in the reference element
                Scalar localArea = scvf.area();
                Scalar flux = fluxVars.molecularDiffusionFlux(1)[0];

                // transform the volume flux into a velocity vector
                Velocity tmpVelocity = localNormal;
                tmpVelocity *= flux;

                scvVelocities[scvf.insideScvIdx()] += tmpVelocity;
                scvVelocities[scvf.outsideScvIdx()] += tmpVelocity;
            }

            // transform vertex velocities from local to global coordinates
            for (auto&& scv : scvs(fvGeometry))
            {
                int vIdxGlobal = scv.dofIndex();

                // calculate the subcontrolvolume velocity by the Piola transformation
                Velocity scvVelocity(0);

                jacobianT2.mtv(scvVelocities[scv.indexInElement()], scvVelocity);
                scvVelocity /= geometry.integrationElement(localPos)*cellNum_[vIdxGlobal];
                // add up the wetting phase subcontrolvolume velocities for each vertex
                diffusiveFlux_[vIdxGlobal] += scvVelocity;
            }

        }

    }

    const std::vector<Dune::FieldVector<Scalar, 2>>&getDiffusiveFlux() const
    { return diffusiveFlux_; }

    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        if(globalPos[0] < eps_ || globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_))
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }



    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        Scalar time = getTime();
        // left boundary: two-phase conditions, atmospheric pressure, almost full water saturation
        // 68.6 degree C;
        // we could use another phase presence (onlyWater) on the left, here simplified for better
        // convergence behavior

        // rechter Rand: konstante Temperatur und Sättigung
        // linker Rand: zeitabhängige Temperatur und Sättigung

     if (globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_))
     {
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::temperatureIdx] = 303.15;
        values[Indices::switchIdx] = 0.01;
        values.setState(Indices::bothPhases);

     }
  else
     {
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 1;
        values[Indices::temperatureIdx] = 318.15+35*sin(time*2*M_PI/86400);
        values.setState(Indices::bothPhases);
        //std::cout<<time<<std::endl;

    }
        return values;
    }

    NumEqVector neumannAtPos( const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        return values;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
      return initial_(globalPos);
    }

    //! Set the current time at which we evaluate the source
    void setTime(Scalar time)
    { time_ = time; }

    //! Set the current time at which we evaluate the source
    Scalar getTime() const
    { return time_; }

private:

Scalar time_ = 0.0;

    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        // since we are interested in a steady-state,
        // the initial conditions serve mainly to illustrate the way towards the
        // steady state, but they may be changed as desired
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 0.1053;
        values[Indices::temperatureIdx] = 283.15;
        values.setState(Indices::bothPhases);

        return values;
    }

    std::vector<Dune::FieldVector<Scalar, 2>> diffusiveFlux_;
    std::vector<int> cellNum_;
    static constexpr Scalar eps_ = 1e-6;
//TimeLoopPtr timeLoop_;

};

} // namespace Dumux

#endif
