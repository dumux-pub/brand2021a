// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
#define DUMUX_HEATPIPE_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>

#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotpckrsw.hh>

namespace Dumux
{

template<class FVGridGeometry, class Scalar>
class HeatPipeSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, HeatPipeSpatialParams<FVGridGeometry, Scalar>>

{
    using ThisType = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    //using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using PcKrSw = FluidMatrix::VanGenuchtenDefault<Scalar>;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;

public:
    using PermeabilityType = Scalar;


    HeatPipeSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    ,pcKrSwCurve_liner1("SpatialParams.liner1")
    ,pcKrSwCurve_liner2("SpatialParams.liner2")
    ,pcKrSwCurve_insulation("SpatialParams.insulation")
    {
        permeability_liner1 = getParam<Scalar>("Problem.Permeability_liner1");
        permeability_liner2 = getParam<Scalar>("Problem.Permeability_liner2");


        porosity_liner1 = 0.000001;

        linerthickness1_ = 0.0025;

        porosity_liner2 = 0.000001;

        linerthickness2_ = 0.0025;

        permeability_insulation = getParam<Scalar>("Problem.Permeability_insulation");

        porosity_insulation = 0.95;

        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");

    }
    
            void plotMaterialLaw()
    {
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);

        const auto sw = linspace(0.0, 1.0, 1000);

        const auto pc = samplePcSw(pcKrSwCurve_insulation, sw);
        Gnuplot::addPcSw(gnuplot, sw, pc, "pc-Sw", "w lp");
        gnuplot.setOption("set xrange [0:1]");
        gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");

        gnuplot.resetAll();

        const auto [krw, krn] = sampleRelPerms(makeFluidMatrixInteraction(pcKrSwCurve_insulation), sw); // test wrapped law
        Gnuplot::addRelPerms(gnuplot, sw, krw, krn, "kr-Sw", "w lp");
        gnuplot.plot("kr");
    }

    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        const auto& globalPos = scv.dofPosition();
        if (compIdx == SolidSystem::comp1Idx)
        {
            if (isInliner1_(globalPos))
                return 1-porosity_liner1;
            else
                return 0;
        }
        else if (compIdx == SolidSystem::comp2Idx)
        {
            if (isInliner2_(globalPos))
                return 1-porosity_liner2;
            else
                return 0;
        }
        else
        {
            if (isInliner1_(globalPos) || isInliner2_(globalPos))
                return 0;
            else
                return 1-porosity_insulation;
        }
    }



    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {

        if (isInliner1_(globalPos))
        {
            return permeability_liner1;
        }
        else if (isInliner2_(globalPos))
        {
            return permeability_liner2;
        }
        else
        {
            return permeability_insulation;
        }
    }

    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        if (isInliner1_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_liner1);
        else if (isInliner2_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_liner2);
        else
            return makeFluidMatrixInteraction(pcKrSwCurve_insulation);
    }


    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }




private:


    static constexpr Scalar eps_ = 1e-6;

    // provides a convenient way distinguishing whether a given location is inside the aquitard
    bool isInliner1_(const GlobalPosition &globalPos) const
    {

        return globalPos[0] < linerthickness1_ + eps_;
    }

    bool isInliner2_(const GlobalPosition &globalPos) const
    {
        // globalPos[dimWorld-1] is the y direction for 2D grids or the z direction for 3D grids
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - linerthickness2_+ eps_;
    }



    PermeabilityType permeability_insulation;


Scalar porosity_insulation;
Scalar Swr_insulation;
Scalar Swn_insulation;
Scalar VanGenuchtenN_insulation;
Scalar VanGenuchtenAlpha_insulation;

    Scalar porosity_liner1;
    Scalar linerthickness1_;
    PermeabilityType permeability_liner1;

    Scalar Swr_liner1;
    Scalar Swn_liner1;
    Scalar VanGenuchtenN_liner1;
    Scalar VanGenuchtenAlpha_liner1;

    Scalar porosity_liner2;
    Scalar linerthickness2_;
    PermeabilityType permeability_liner2;

    Scalar Swr_liner2;
    Scalar Swn_liner2;
    Scalar VanGenuchtenN_liner2;
    Scalar VanGenuchtenAlpha_liner2;




    const PcKrSw pcKrSwCurve_liner1;
    const PcKrSw pcKrSwCurve_liner2;
    const PcKrSw pcKrSwCurve_insulation;


    bool plotFluidMatrixInteractions_;
};

}

#endif
