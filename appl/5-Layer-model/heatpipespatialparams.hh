// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
#define DUMUX_HEATPIPE_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>

#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotpckrsw.hh>

namespace Dumux
{

template<class FVGridGeometry, class Scalar>
class HeatPipeSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, HeatPipeSpatialParams<FVGridGeometry, Scalar>>

{
    using ThisType = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    //using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using PcKrSw = FluidMatrix::VanGenuchtenDefault<Scalar>;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;

public:
    using PermeabilityType = Scalar;


    HeatPipeSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    ,pcKrSwCurve_layer1("SpatialParams.layer1")
    ,pcKrSwCurve_layer2("SpatialParams.layer2")
    ,pcKrSwCurve_layer3("SpatialParams.layer3")
    ,pcKrSwCurve_layer4("SpatialParams.layer4")
    ,pcKrSwCurve_layer5("SpatialParams.layer5")

    {
        permeability_layer1 = getParam<Scalar>("Problem.Permeability_layer1");
        permeability_layer2 = getParam<Scalar>("Problem.Permeability_layer2");
        permeability_layer3 = getParam<Scalar>("Problem.Permeability_layer3");
        permeability_layer4 = getParam<Scalar>("Problem.Permeability_layer4");
        permeability_layer5 = getParam<Scalar>("Problem.Permeability_layer5");

        porosity_layer1 = getParam<Scalar>("Problem.porosity_layer1");
        porosity_layer2 = getParam<Scalar>("Problem.porosity_layer2");
        porosity_layer3 = getParam<Scalar>("Problem.porosity_layer3");
        porosity_layer4 = getParam<Scalar>("Problem.porosity_layer4");
        porosity_layer5 = getParam<Scalar>("Problem.porosity_layer5");

        linerthickness_layer1 = getParam<Scalar>("Problem.linerthickness_layer1");
        linerthickness_layer2 = getParam<Scalar>("Problem.linerthickness_layer2");
        linerthickness_layer3 = getParam<Scalar>("Problem.linerthickness_layer3");
        linerthickness_layer4 = getParam<Scalar>("Problem.linerthickness_layer4");
        linerthickness_layer5 = getParam<Scalar>("Problem.linerthickness_layer5");


        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");
    }
            void plotMaterialLaw()
    {
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);

        const auto sw = linspace(0.0, 1.0, 1000);

        const auto pc = samplePcSw(pcKrSwCurve_layer3, sw);
        Gnuplot::addPcSw(gnuplot, sw, pc, "pc-Sw", "w lp");
        gnuplot.setOption("set xrange [0:1]");
        gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");

        gnuplot.resetAll();

        const auto [krw, krn] = sampleRelPerms(makeFluidMatrixInteraction(pcKrSwCurve_layer3), sw); // test wrapped law
        Gnuplot::addRelPerms(gnuplot, sw, krw, krn, "kr-Sw", "w lp");
        gnuplot.plot("kr");
    }

    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        const auto& globalPos = scv.dofPosition();
        if (compIdx == SolidSystem::comp1Idx)
        {
            if (isInlayer1_(globalPos))
                return 1-porosity_layer1;
            else
                return 0;
        }
        else if (compIdx == SolidSystem::comp2Idx)
        {
            if (isInlayer2_(globalPos))
                return 1-porosity_layer2;
            else
                return 0;
        }
        else if (compIdx == SolidSystem::comp3Idx)
        {
            if (isInlayer3_(globalPos))
                return 1-porosity_layer3;
            else
                return 0;
        }
        else if (compIdx == SolidSystem::comp4Idx)
        {
            if (isInlayer4_(globalPos))
                return 1-porosity_layer4;
            else
                return 0;
        }
        else
        {
            if (isInlayer1_(globalPos) || isInlayer2_(globalPos) || isInlayer4_(globalPos) || isInlayer4_(globalPos))
                return 0;
            else
                return 1-porosity_layer5;
        }
    }



    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {

        if (isInlayer1_(globalPos))
        {
            return permeability_layer1;
        }
        else if (isInlayer2_(globalPos))
        {
            return permeability_layer2;
        }

        else if (isInlayer3_(globalPos))
        {
            return permeability_layer3;
        }

        else if (isInlayer4_(globalPos))
        {
            return permeability_layer4;
        }

        else
        {
            return permeability_layer5;
        }
    }

    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        if (isInlayer1_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_layer1);

        else if (isInlayer2_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_layer2);

        else if (isInlayer3_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_layer3);

        else if (isInlayer4_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurve_layer4);

        else
            return makeFluidMatrixInteraction(pcKrSwCurve_layer5);
    }


    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }




private:


    static constexpr Scalar eps_ = 1e-6;

    // provides a convenient way distinguishing whether a given location is inside the aquitard
    bool isInlayer1_(const GlobalPosition &globalPos) const
    {

        return globalPos[0] < linerthickness_layer1 + eps_;
    }

    bool isInlayer2_(const GlobalPosition &globalPos) const
    {

        return globalPos[0] < this->gridGeometry().bBoxMax()[0] - linerthickness_layer5 - linerthickness_layer4 - linerthickness_layer3 - eps_ && globalPos[0] > linerthickness_layer1 + eps_;
    }


    bool isInlayer3_(const GlobalPosition &globalPos) const
    {
        // globalPos[dimWorld-1] is the y direction for 2D grids or the z direction for 3D grids
        return globalPos[0] < this->gridGeometry().bBoxMax()[0] - linerthickness_layer5 - linerthickness_layer4 - eps_ && globalPos[0] > linerthickness_layer1 + linerthickness_layer2 + eps_;
    }


    bool isInlayer4_(const GlobalPosition &globalPos) const
    {
        // globalPos[dimWorld-1] is the y direction for 2D grids or the z direction for 3D grids
        return globalPos[0] < this->gridGeometry().bBoxMax()[0] - linerthickness_layer5 - eps_ && globalPos[0] > linerthickness_layer1 + linerthickness_layer2 + linerthickness_layer3 + eps_;
    }


    bool isInlayer5_(const GlobalPosition &globalPos) const
    {
        // globalPos[dimWorld-1] is the y direction for 2D grids or the z direction for 3D grids
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - linerthickness_layer5 - eps_;
    }




Scalar porosity_layer1;
PermeabilityType permeability_layer1;
Scalar linerthickness_layer1;

Scalar Swr_layer1;
Scalar Swn_layer1;
Scalar VanGenuchtenN_layer1;
Scalar VanGenuchtenAlpha_layer1;


Scalar porosity_layer2;
PermeabilityType permeability_layer2;
Scalar linerthickness_layer2;

Scalar Swr_layer2;
Scalar Swn_layer2;
Scalar VanGenuchtenN_layer2;
Scalar VanGenuchtenAlpha_layer2;

Scalar porosity_layer3;
PermeabilityType permeability_layer3;
Scalar linerthickness_layer3;

Scalar Swr_layer3;
Scalar Swn_layer3;
Scalar VanGenuchtenN_layer3;
Scalar VanGenuchtenAlpha_layer3;

Scalar porosity_layer4;
PermeabilityType permeability_layer4;
Scalar linerthickness_layer4;

Scalar Swr_layer4;
Scalar Swn_layer4;
Scalar VanGenuchtenN_layer4;
Scalar VanGenuchtenAlpha_layer4;

Scalar porosity_layer5;
PermeabilityType permeability_layer5;
Scalar linerthickness_layer5;

Scalar Swr_layer5;
Scalar Swn_layer5;
Scalar VanGenuchtenN_layer5;
Scalar VanGenuchtenAlpha_layer5;




    const PcKrSw pcKrSwCurve_layer1;
    const PcKrSw pcKrSwCurve_layer2;
    const PcKrSw pcKrSwCurve_layer3;
    const PcKrSw pcKrSwCurve_layer4;
    const PcKrSw pcKrSwCurve_layer5;


    bool plotFluidMatrixInteractions_;
};

}

#endif
