// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_PROPERTIES_HH
#define DUMUX_HEATPIPE_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>

#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/porousmediumflow/2p2c/model.hh>

#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "heatpipespatialparams.hh"
#include "heatpipeproblem.hh"

namespace Dumux::Properties
{
// Create new type tags
namespace TTag {
struct HeatPipeTypeTag { using InheritsFrom = std::tuple<TwoPTwoCNI, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::HeatPipeTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::HeatPipeTypeTag> { using type = HeatPipeProblem<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::HeatPipeTypeTag> { using type = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>; };

// pn-sw formulation
template<class TypeTag>
struct Formulation<TypeTag, TTag::HeatPipeTypeTag> { static constexpr auto value = TwoPFormulation::p1s0; };

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::HeatPipeTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Dumux::Components::Constant<1, Scalar>;
    using ComponentTwo = Dumux::Components::Constant<2, Scalar>;
    using ComponentThree = Dumux::Components::Constant<3, Scalar>;
    using ComponentFour = Dumux::Components::Constant<4, Scalar>;
    using ComponentFive = Dumux::Components::Constant<5, Scalar>;
    static constexpr int numInertComponents = 5;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, ComponentThree, ComponentFour, ComponentFive, numInertComponents>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::HeatPipeTypeTag>

{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using type = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
};

} // namespace Dumux::Properties

#endif
