 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_PROBLEM_HH
#define DUMUX_HEATPIPE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux
{
template <class TypeTag >
class HeatPipeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem =GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using VolumeVariables = typename GridVariables::GridVolumeVariables::VolumeVariables;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

public:
    HeatPipeProblem(std::shared_ptr<const FVGridGeometry> fVGridGeometry)
    : ParentType(fVGridGeometry)
    {
        FluidSystem::init();
        this->spatialParams().plotMaterialLaw();
    }

        /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */
    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.solidThermalConductivity(); }, "solidThermalConductivity");
    }

    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        if(globalPos[0] < eps_ || globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_))
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }



    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        Scalar time = getTime();
        // left boundary: two-phase conditions, atmospheric pressure, almost full water saturation
        // 68.6 degree C;
        // we could use another phase presence (onlyWater) on the left, here simplified for better
        // convergence behavior

        // rechter Rand: konstante Temperatur und Sättigung
        // linker Rand: zeitabhängige Temperatur und Sättigung

     if (globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_))
     {
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::temperatureIdx] = 303.15;
        values[Indices::switchIdx] = 0.01;
        values.setState(Indices::bothPhases);

     }
  else
     {
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 1;
        values[Indices::temperatureIdx] = 318.15+35*sin(time*2*M_PI/86400);
        values.setState(Indices::bothPhases);
        //std::cout<<time<<std::endl;


    }
        return values;
    }

    NumEqVector neumannAtPos( const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        return values;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
      return initial_(globalPos);
    }

    //! Set the current time at which we evaluate the source
    void setTime(Scalar time)
    { time_ = time; }

    //! Set the current time at which we evaluate the source
    Scalar getTime() const
    { return time_; }

private:

Scalar time_ = 0.0;

    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        // since we are interested in a steady-state,
        // the initial conditions serve mainly to illustrate the way towards the
        // steady state, but they may be changed as desired
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 0.1053;
        values[Indices::temperatureIdx] = 283.15;
        values.setState(Indices::bothPhases);

        return values;
    }

    static constexpr Scalar eps_ = 1e-6;
//TimeLoopPtr timeLoop_;

};

} // namespace Dumux

#endif
