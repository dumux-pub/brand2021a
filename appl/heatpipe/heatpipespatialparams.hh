// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
#define DUMUX_HEATPIPE_SPATIAL_PARAMS_HH


#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>
#include <dumux/io/plotmateriallaw3p.hh>
#include <dumux/io/plotpckrsw.hh>
#include <dumux/io/gnuplotinterface.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

namespace Dumux
{

template<class FVGridGeometry, class Scalar>
class HeatPipeSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, HeatPipeSpatialParams<FVGridGeometry, Scalar>>

{
    using ThisType = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using PcKrSw = FluidMatrix::VanGenuchtenDefault<Scalar>;

public:
    using PermeabilityType = Scalar;

    HeatPipeSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , pcKrSwCurve_("SpatialParams")
    {
        permeability_ = getParam<Scalar>("Problem.Permeability");
        porosity_ = 0.95;
        tortuosity_ = getParam<Scalar>("SpatialParams.Tortuosity");


        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");
    }

    void plotMaterialLaw()
    {
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);

        const auto sw = linspace(0.0, 1.0, 1000);

        const auto pc = samplePcSw(pcKrSwCurve_, sw);
        Gnuplot::addPcSw(gnuplot, sw, pc, "pc-Sw", "w lp");
        gnuplot.setOption("set xrange [0:1]");
       // gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");

        gnuplot.resetAll();

        const auto [krw, krn] = sampleRelPerms(makeFluidMatrixInteraction(pcKrSwCurve_), sw); // test wrapped law
        Gnuplot::addRelPerms(gnuplot, sw, krw, krn, "kr-Sw", "w lp");
        gnuplot.plot("kr");
    }



    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return makeFluidMatrixInteraction(pcKrSwCurve_); }


    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:
    PermeabilityType permeability_;
    Scalar porosity_;
    Scalar tortuosity_;
    Scalar Swr_;
    Scalar Swn_;
    Scalar VanGenuchtenN_;
    Scalar VanGenuchtenAlpha_;
    const PcKrSw pcKrSwCurve_;
    bool plotFluidMatrixInteractions_;
};

}

#endif

