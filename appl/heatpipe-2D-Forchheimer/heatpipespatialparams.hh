// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
#define DUMUX_HEATPIPE_SPATIAL_PARAMS_HH


#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>
#include <dumux/io/plotmateriallaw3p.hh>
#include <dumux/io/plotpckrsw.hh>
#include <dumux/io/gnuplotinterface.hh>


#include <random>
#include <dumux/common/random.hh>

#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

namespace Dumux
{

template<class FVGridGeometry, class Scalar>
class HeatPipeSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, HeatPipeSpatialParams<FVGridGeometry, Scalar>>

{
    using ThisType = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using PcKrSw = FluidMatrix::VanGenuchtenDefault<Scalar>;

 
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
  
    static constexpr int dimWorld = GridView::dimensionworld;
    //using GlobalPosition = typename SubControlVolume::GlobalPosition;




public:
    using PermeabilityType = Scalar;

    HeatPipeSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , pcKrSwCurve_("SpatialParams")
    , P_(fvGridGeometry->gridView().size(0), 0.0)
    {
//         The permeability of the domain and the lens are obtained from the `params.input` file.
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");
        permeabilityLens_ = getParam<Scalar>("SpatialParams.PermeabilityLens");
        porosity_ = 0.35;
        
        porosityLens_ = 0.35;
        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");
        // Furthermore, the position of the lens, which is defined by the position of the lower left and the upper right corners, are obtained from the input file.
        lensLowerLeft_ = getParam<GlobalPosition>("SpatialParams.LensLowerLeft");
        lensUpperRight_ = getParam<GlobalPosition>("SpatialParams.LensUpperRight");

        lensK_ = getParam<Scalar>("SpatialParams.Lens.Permeability", 1e-7);
        outerK_ = getParam<Scalar>("SpatialParams.Outer.Permeability", 1.02e-9);

        referencePorosity_     = getParam<Scalar>("SpatialParams.referencePorosity", 0.35);
        referencePermeability_ = getParam<Scalar>("SpatialParams.referencePermeability", 1e-6);



        std::mt19937 rand(0);

        Dumux::SimpleLogNormalDistribution<Scalar> P(std::log(porosity_), std::log(porosity_)*0.1);
        Dumux::SimpleLogNormalDistribution<Scalar> PLens(std::log(porosityLens_), std::log(porosityLens_)*0.1);


        for (const auto& element : elements(fvGridGeometry->gridView()))
        {
            const auto eIdx = fvGridGeometry->elementMapper().index(element);
            const auto globalPos = element.geometry().center();
            P_[eIdx] = isInLens_(globalPos) ? PLens(rand) : P(rand);
        }
    
    }

    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }
 
    

    void plotMaterialLaw()
    {
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);

        const auto sw = linspace(0.0, 1.0, 1000);

        const auto pc = samplePcSw(pcKrSwCurve_, sw);
        Gnuplot::addPcSw(gnuplot, sw, pc, "pc-Sw", "w lp");
        gnuplot.setOption("set xrange [0:1]");
       // gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");

        gnuplot.resetAll();

        const auto [krw, krn] = sampleRelPerms(makeFluidMatrixInteraction(pcKrSwCurve_), sw); // test wrapped law
        Gnuplot::addRelPerms(gnuplot, sw, krw, krn, "kr-Sw", "w lp");
        gnuplot.plot("kr");
    }

///////////////////////// HOMOGENE POROSITY /////////////////////////////

/*
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        return porosity_;
    }

*/


/////////////////////// HOMOGENE POROSITY ENDE////////////////////////////


///////////////////////// RANDOM PERMEABILITY ////////////////////////////



    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        return P_[scv.elementIndex()];
    }

    const std::vector<Scalar>& getPField() const
    { return P_; }



    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    { return referencePorosity_; }


    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {

        const auto poro = P_[scv.elementIndex()];
        return permLaw_.evaluatePermeability(referencePermeability_, referencePorosity_, poro);
    }



//////////////////////////RANDOM PERMEABILITY ENDE///////////////////////////////


////////// Für Definierte Linse  //////////
/*

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
     {



         if (isInLens_(globalPos))
          return porosity_Lens;
        return porosity_;
    }
*/

/////////////////////////////////////////////

    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return makeFluidMatrixInteraction(pcKrSwCurve_); }


    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:

    PermeabilityType permeability_;

    Scalar porosity_;
    Scalar porosityLens_;
    Scalar Swr_;
    Scalar Swn_;
    Scalar VanGenuchtenN_;
    Scalar VanGenuchtenAlpha_;
    const PcKrSw pcKrSwCurve_;
    bool plotFluidMatrixInteractions_;


    bool isInLens_(const GlobalPosition& globalPos) const
    {
        for (int i = 0; i < dimWorld; ++i)
        {
            if (globalPos[i] < lensLowerLeft_[i] + 1.5e-7
                || globalPos[i] > lensUpperRight_[i] - 1.5e-7)
                return false;
        }

        return true;
    }

    GlobalPosition lensLowerLeft_, lensUpperRight_;

    Scalar lensK_;
    Scalar outerK_;


    Scalar permeabilityLens_;
    std::vector<Scalar> P_;

//    Scalar permeability_;  
    PermeabilityKozenyCarman<PermeabilityType> permLaw_;
    PermeabilityType referencePermeability_ = 0.0;
    Scalar referencePorosity_;



};

}

#endif
