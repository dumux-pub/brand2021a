// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_PROBLEM_HH
#define DUMUX_HEATPIPE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux
{

template <class TypeTag>
// class Scalar , class MaterialLaw>

class HeatPipeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem =GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using VolumeVariables = typename GridVariables::GridVolumeVariables::VolumeVariables;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;


public:

    HeatPipeProblem(std::shared_ptr<const FVGridGeometry> fVGridGeometry)
    : ParentType(fVGridGeometry)
    {
        FluidSystem::init();
        heattransfercoefficientstorage_ = getParam<Scalar>("Problem.HeatTransferCoefficientStorage");
        heattransfercoefficientambient_ = getParam<Scalar>("Problem.HeatTransferCoefficientAmbient");

        this->spatialParams().plotMaterialLaw();
    }


    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.effectiveThermalConductivity(); }, "effectiveThermalConductivity");
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.permeability(); }, "Permeability");
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.effectiveDiffusionCoefficient(1/*phaseIdx*/,1/*compIIdx*/,/*compJIdx*/0); }, "effectiveDiffusionCoefficient");
        vtk.addVolumeVariable([](const VolumeVariables& v){ return v.viscosity (1); },"viscosity");
    }



    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    Scalar time = getTime();



    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

            values.setAllNeumann();

        return values;
    }


    //we set Neumann everywhere, this is not used at the moment
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     * Negative values mean influx.
     */
    template<class ElementVolumeVariables, class ElementFluxVariablesCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        Scalar time = getTime();
        const auto globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        if (globalPos[1] > (this->gridGeometry().bBoxMax()[1] - eps_))
      
        {
            const Scalar dirichletTemp = 298.15;

            // evaluate the temperature gradient
            GlobalPosition gradT(0.0);
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto xIp = scv.dofPosition()[1];
                auto tmp = fluxVarsCache.gradN(scv.localDofIndex());
                tmp *= xIp > this->gridGeometry().bBoxMax()[1] - eps_ ? dirichletTemp
                                     : elemVolVars[scv].temperature();
                gradT += tmp;
            }

            // compute flux
            auto energyFluxUp = vtmv(scvf.unitOuterNormal(), volVars.effectiveThermalConductivity(), gradT);

            values[Indices::energyEqIdx] = -energyFluxUp;
            if(time>86000)
            {	
            std::cout<<"energyfluxUp   "<<values[Indices::energyEqIdx]<<std::endl;
        	}
        }
        else if (globalPos[1] < (this->gridGeometry().bBoxMin()[1] + eps_))

        {
            const Scalar dirichletTemp = 348.15;//+35*sin(time*2*M_PI/86400); // Temperatur im Modell nach Ochs ist um Phasenverschoben und hinkt 2 h zurück, start punkt bei Ochs: 30 °C und nicht 45

            // evaluate the temperature gradient
            GlobalPosition gradT(0.0);
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto xIp = scv.dofPosition()[1];
                auto tmp = fluxVarsCache.gradN(scv.localDofIndex());
                tmp *= xIp < this->gridGeometry().bBoxMin()[1] + eps_ ? dirichletTemp
                                        : elemVolVars[scv].temperature();
                gradT += tmp;
            }

            // compute flux
            auto energyFluxDown = vtmv(scvf.unitOuterNormal(), volVars.effectiveThermalConductivity(), gradT);

            values[Indices::energyEqIdx] = -energyFluxDown;

      		if (time>86000)
      		{	
			std::cout<<"energyfluxDown   "<<values[Indices::energyEqIdx]<<std::endl;
			}
        }

        
        return values;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
      return initial_(globalPos);
    }

    //! Set the current time at which we evaluate the source
    void setTime(Scalar time)
    { time_ = time; }

    //! Set the current time at which we evaluate the source
    Scalar getTime() const
    { return time_; }

private:

Scalar time_ = 0.0;
Scalar heattransfercoefficientstorage_ = 0.0;
Scalar heattransfercoefficientambient_ = 0.0;

    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 0.01435; //2 kg mit roh = 996 und Poro = 0.35 => 0.00574 //10kg mit 0.0287 //0.0001 für 0
        values[Indices::temperatureIdx] = 298.15;
        values.setState(Indices::bothPhases);

        return values;
    }

    static constexpr Scalar eps_ = 1e-6;
//TimeLoopPtr timeLoop_;

};

} // namespace Dumux

#endif
