SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

T. Brand <br>
Numerische Simulation des wärmegekoppelten Stofftransports durch die Speicherhülle eines Erdbeckenwärmespeichers<br>
Master's Thesis, 2022<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and download `install_brand2021a.py` from this repository.

```
mkdir brand2021a && cd brand2021a
python3 install_brand2021a.py
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.



After the script has run successfully, you may build all executables

```bash
cd DUMUX/brand2021a/build-cmake
make build_tests
```
